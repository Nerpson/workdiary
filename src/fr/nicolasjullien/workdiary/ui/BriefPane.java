package fr.nicolasjullien.workdiary.ui;

import fr.nicolasjullien.workdiary.model.Done;
import fr.nicolasjullien.workdiary.model.Todo;
import javafx.scene.layout.BorderPane;

public class BriefPane extends BorderPane {

    private final Done done;
    private final Todo todo;

    private final BriefSection doneBriefSection;
    private final BriefSection todoBriefSection;

    public BriefPane(Done done, Todo todo) {
        this.done = done;
        this.todo = todo;

        this.doneBriefSection = new BriefSection(done.getText());
        this.todoBriefSection = new BriefSection(todo.getText());

        this.setLeft(this.doneBriefSection);
        this.setRight(this.todoBriefSection);

        this.done.addTextChangedListener(e -> this.doneBriefSection.setText(e.text));
        this.todo.addTextChangedListener(e -> this.todoBriefSection.setText(e.text));
    }

}
