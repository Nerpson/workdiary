package fr.nicolasjullien.workdiary.ui;

import fr.nicolasjullien.workdiary.Controller;
import fr.nicolasjullien.workdiary.model.Day;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.CornerRadii;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class DayPane extends BorderPane {

    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("eeee d MMMM u");

    private final Day day;
    private final BriefPane briefPane;
    public final Label dayLabel;

    public DayPane(Day day) {
        this.day = day;

        this.setMinHeight(70);
        this.setPadding(new Insets(10, 10, 10, 10));

        this.dayLabel = new Label(this.dateToText(this.day.getDate()));
        this.dayLabel.setFont(Font.font(18.));
        this.briefPane = new BriefPane(this.day.getDone(), this.day.getTodo());

        this.setTop(this.dayLabel);
        this.setCenter(this.briefPane);

        Controller controller = Controller.getInstance();
        controller.addDaySelectionChangedListener(e -> updateDisplay(e.day == this.day));
        this.setOnMouseClicked(e -> {
            controller.selectDay(this.day);
            e.consume();
        });

        this.day.addOnDateChangedListener(e -> {
            this.dayLabel.setText(this.dateToText(e.date));
        });

        updateDisplay(controller.getSelectedDay() == this.day);
    }

    private void updateDisplay(boolean isSelected) {
        if (isSelected) {
            this.setBackground(new Background(new BackgroundFill(Color.LIGHTBLUE, CornerRadii.EMPTY, Insets.EMPTY)));
        } else {
            this.setBackground(new Background(new BackgroundFill(Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY)));
        }
    }

    private String dateToText(LocalDate date) {
        String dateText = date.format(formatter);
        return dateText.substring(0, 1).toUpperCase() + dateText.substring(1);
    }

}
