package fr.nicolasjullien.workdiary.ui;

import fr.nicolasjullien.workdiary.model.Done;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;

public class DonePane extends BorderPane {

    private final Label doneLabel;
    private final TextArea textArea;

    private Done done;

    public DonePane() {
        this.doneLabel = new Label("Done");
        this.textArea = new TextArea();
        this.textArea.setDisable(true);
        this.textArea.textProperty().addListener((observable, oldValue, newValue) -> {
            if (this.done != null) this.done.setText(newValue);
        });

        this.setTop(this.doneLabel);
        this.setCenter(this.textArea);
    }

    public void setDone(Done done) {
        this.done = done;
        if (this.done != null) {
            this.textArea.setDisable(false);
            this.textArea.setText(done.getText());
        } else {
            this.textArea.setDisable(true);
            this.textArea.clear();
        }
    }
}
