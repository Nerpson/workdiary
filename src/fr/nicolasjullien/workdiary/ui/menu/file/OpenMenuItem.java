package fr.nicolasjullien.workdiary.ui.menu.file;

import fr.nicolasjullien.workdiary.Controller;
import javafx.scene.control.MenuItem;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;

public class OpenMenuItem extends MenuItem {

    public OpenMenuItem() {
        super("Open");
        this.setAccelerator(new KeyCodeCombination(KeyCode.O, KeyCodeCombination.CONTROL_DOWN));

        this.setOnAction(e -> Controller.getInstance().openFile());
    }

}
