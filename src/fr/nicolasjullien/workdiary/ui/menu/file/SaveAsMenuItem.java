package fr.nicolasjullien.workdiary.ui.menu.file;

import fr.nicolasjullien.workdiary.Controller;
import javafx.scene.control.MenuItem;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;

public class SaveAsMenuItem extends MenuItem {

    public SaveAsMenuItem() {
        super("Save as...");
        this.setAccelerator(new KeyCodeCombination(KeyCode.S, KeyCodeCombination.CONTROL_DOWN, KeyCodeCombination.SHIFT_DOWN));

        this.setOnAction(e -> Controller.getInstance().saveFileAs());
    }

}
