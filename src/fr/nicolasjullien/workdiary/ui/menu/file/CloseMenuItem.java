package fr.nicolasjullien.workdiary.ui.menu.file;

import javafx.scene.control.MenuItem;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;

public class CloseMenuItem extends MenuItem {

    public CloseMenuItem() {
        super("Close");
        this.setAccelerator(new KeyCodeCombination(KeyCode.F4, KeyCodeCombination.ALT_DOWN));
    }

}
