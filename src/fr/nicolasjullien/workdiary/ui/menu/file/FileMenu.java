package fr.nicolasjullien.workdiary.ui.menu.file;

import javafx.scene.control.Menu;

public class FileMenu extends Menu {

    private final NewMenuItem newMenuItem;
    private final OpenMenuItem openMenuItem;
    private final SaveMenuItem saveMenuItem;
    private final SaveAsMenuItem saveAsMenuItem;
    private final CloseMenuItem closeMenuItem;

    public FileMenu() {
        super("File");

        this.newMenuItem = new NewMenuItem();
        this.openMenuItem = new OpenMenuItem();
        this.saveMenuItem = new SaveMenuItem();
        this.saveAsMenuItem = new SaveAsMenuItem();
        this.closeMenuItem = new CloseMenuItem();

        getItems().addAll(
                this.newMenuItem,
                this.openMenuItem,
                this.saveMenuItem,
                this.saveAsMenuItem,
                this.closeMenuItem
        );
    }
}
