package fr.nicolasjullien.workdiary.ui.menu.file;

import fr.nicolasjullien.workdiary.Controller;
import javafx.scene.control.MenuItem;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;

public class NewMenuItem extends MenuItem {

    public NewMenuItem() {
        super("New");
        this.setAccelerator(new KeyCodeCombination(KeyCode.N, KeyCodeCombination.CONTROL_DOWN));

        this.setOnAction(e -> Controller.getInstance().newFile());
    }
}
