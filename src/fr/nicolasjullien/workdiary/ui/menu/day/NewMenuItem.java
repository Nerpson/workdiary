package fr.nicolasjullien.workdiary.ui.menu.day;

import fr.nicolasjullien.workdiary.Controller;
import javafx.scene.control.MenuItem;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;

public class NewMenuItem extends MenuItem {

    public NewMenuItem() {
        super("New");
        this.setAccelerator(new KeyCodeCombination(KeyCode.A, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN));

        this.setOnAction(e -> {
            Controller.getInstance().addDay();
        });
    }

}
