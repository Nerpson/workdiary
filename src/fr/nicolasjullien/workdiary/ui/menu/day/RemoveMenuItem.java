package fr.nicolasjullien.workdiary.ui.menu.day;

import fr.nicolasjullien.workdiary.Controller;
import javafx.scene.control.MenuItem;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;

public class RemoveMenuItem extends MenuItem {

    public RemoveMenuItem() {
        super("Remove");
        this.setDisable(true);
        this.setAccelerator(new KeyCodeCombination(KeyCode.D, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN));

        Controller controller = Controller.getInstance();
        this.setOnAction(e -> controller.removeDay(controller.getSelectedDay()));
        controller.addDaySelectionChangedListener(e -> {
            this.setDisable(e.day == null);
        });
    }

}
