package fr.nicolasjullien.workdiary.ui.menu.day;

import javafx.scene.control.Menu;

public class DayMenu extends Menu {

    private final NewMenuItem newMenuItem;
    private final RemoveMenuItem removeMenuItem;

    public DayMenu() {
        super("Day");

        this.newMenuItem = new NewMenuItem();
        this.removeMenuItem = new RemoveMenuItem();

        getItems().addAll(this.newMenuItem, this.removeMenuItem);
    }

}
