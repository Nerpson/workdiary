package fr.nicolasjullien.workdiary.ui.menu;

import fr.nicolasjullien.workdiary.ui.menu.day.DayMenu;
import fr.nicolasjullien.workdiary.ui.menu.file.FileMenu;

public class MenuBar extends javafx.scene.control.MenuBar {

    private final FileMenu fileMenu;
    private final DayMenu dayMenu;

    public MenuBar() {
        this.fileMenu = new FileMenu();
        this.dayMenu = new DayMenu();
        getMenus().addAll(this.fileMenu, this.dayMenu);
    }
}
