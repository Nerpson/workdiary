package fr.nicolasjullien.workdiary.ui;

import fr.nicolasjullien.workdiary.model.Todo;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;

public class TodoPane extends BorderPane {

    private final Label todoLabel;
    private final TextArea textArea;

    private Todo todo;

    public TodoPane() {
        this.todoLabel = new Label("To do");
        this.textArea = new TextArea();
        this.textArea.setDisable(true);
        this.textArea.textProperty().addListener((observable, oldValue, newValue) -> {
            if (this.todo != null) this.todo.setText(newValue);
        });

        this.setTop(this.todoLabel);
        this.setCenter(this.textArea);
    }

    public void setTodo(Todo todo) {
        this.todo = todo;
        if (this.todo != null) {
            this.textArea.setDisable(false);
            this.textArea.setText(todo.getText());
        } else {
            this.textArea.setDisable(true);
            this.textArea.clear();
        }
    }
}
