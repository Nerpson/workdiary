package fr.nicolasjullien.workdiary.ui;

import fr.nicolasjullien.workdiary.Controller;
import javafx.geometry.Orientation;
import javafx.scene.control.SplitPane;
import javafx.scene.layout.BorderPane;

public class EditPane extends BorderPane {

    private final DatePane datePane;
    private final SplitPane splitPane;
    private final DonePane donePane;
    private final TodoPane todoPane;

    public EditPane() {
        this.datePane = new DatePane();
        this.splitPane = new SplitPane();
        this.donePane = new DonePane();
        this.todoPane = new TodoPane();

        this.splitPane.setOrientation(Orientation.HORIZONTAL);
        this.splitPane.getItems().addAll(this.donePane, this.todoPane);

        this.setTop(this.datePane);
        this.setCenter(this.splitPane);

        Controller.getInstance().addDaySelectionChangedListener(e -> {
            this.donePane.setDone((e.day != null) ? e.day.getDone() : null);
            this.todoPane.setTodo((e.day != null) ? e.day.getTodo() : null);
        });
    }
}
