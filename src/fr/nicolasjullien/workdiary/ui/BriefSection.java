package fr.nicolasjullien.workdiary.ui;

import javafx.scene.control.Label;
import javafx.scene.layout.FlowPane;

public class BriefSection extends FlowPane {
    private final Label todoLabel;

    public BriefSection(String text) {
        this.todoLabel = new Label(text);
        this.getChildren().add(this.todoLabel);
    }

    public void setText(String text) {
        this.todoLabel.setText(text);
    }
}
