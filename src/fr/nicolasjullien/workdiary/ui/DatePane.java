package fr.nicolasjullien.workdiary.ui;

import fr.nicolasjullien.workdiary.Controller;
import javafx.geometry.Orientation;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.layout.FlowPane;

public class DatePane extends FlowPane {

    private final Label label;
    private final DatePicker datePicker;

    public DatePane() {
        this.label = new Label("Date");
        this.datePicker = new DatePicker();

        this.setOrientation(Orientation.HORIZONTAL);
        this.getChildren().addAll(this.label, this.datePicker);
        this.datePicker.setDisable(true);
        Controller controller = Controller.getInstance();

        this.datePicker.setOnAction(e -> {
            if (controller.isDaySelected()) controller.getSelectedDay().setDate(this.datePicker.getValue());
        });

        controller.addDaySelectionChangedListener(e -> {
            if (e.day != null) {
                this.datePicker.setDisable(false);
                this.datePicker.setValue(e.day.getDate());
            } else {
                this.datePicker.setDisable(true);
                this.datePicker.setValue(null);
            }
        });
    }

}
