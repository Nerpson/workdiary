package fr.nicolasjullien.workdiary.ui;

import fr.nicolasjullien.workdiary.Controller;
import fr.nicolasjullien.workdiary.model.Day;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;

public class DaysPane extends GridPane {

    public DaysPane() {
        ColumnConstraints col = new ColumnConstraints(100,100,Double.MAX_VALUE);
        col.setHgrow(Priority.ALWAYS);
        col.setFillWidth(true);
        this.getColumnConstraints().add(col);
        this.setVgap(10);

        Controller controller = Controller.getInstance();
        controller.addNewDayListener(e -> updateDisplay());
        controller.addDayRemovedListener(e -> updateDisplay());
    }

    private void updateDisplay() {
        this.getChildren().clear();
        int i = 0;
        for (Day day : Controller.getInstance().getDays()) {
            this.add(new DayPane(day), 0, i++);
        }
    }
}
