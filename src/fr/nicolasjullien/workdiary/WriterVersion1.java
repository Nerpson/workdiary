package fr.nicolasjullien.workdiary;

import fr.nicolasjullien.workdiary.model.Day;
import javafx.scene.control.Alert;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.time.LocalDate;
import java.util.List;

public class WriterVersion1 extends Writer {

    private static final byte VERSION = 1;

    public WriterVersion1(File file) {
        super(file);
    }

    @Override
    public void write() {
        Controller controller = Controller.getInstance();
        try {
            BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(this.file));
            stream.write(VERSION);

            // Writing the number of days.
            List<Day> days = controller.getDays();
            short daysCount = (short) days.size();
            ByteBuffer daysCountBuffer = ByteBuffer.allocate(2);
            daysCountBuffer.putShort(daysCount);
            stream.write(daysCountBuffer.array());

            for (Day day : days) {
                LocalDate date = day.getDate();

                // Year
                ByteBuffer yearBuffer = ByteBuffer.allocate(2);
                yearBuffer.putShort((short) date.getYear());
                stream.write(yearBuffer.array());

                // Month
                stream.write((byte) date.getMonthValue());

                // Day
                stream.write((byte) date.getDayOfMonth());

                // Done
                String doneText = day.getDone().getText();

                byte[] doneTextBytes = doneText.getBytes(Charset.forName("UTF-16"));
                ByteBuffer doneLengthBuffer = ByteBuffer.allocate(2);
                doneLengthBuffer.putShort((short) doneTextBytes.length);
                stream.write(doneLengthBuffer.array());
                stream.write(doneTextBytes);

                // Todo
                String todoText = day.getTodo().getText();

                byte[] todoTextBytes = todoText.getBytes(Charset.forName("UTF-16"));
                ByteBuffer todoLengthBuffer = ByteBuffer.allocate(2);
                todoLengthBuffer.putShort((short) todoTextBytes.length);
                stream.write(todoLengthBuffer.array());
                stream.write(todoTextBytes);
            }

            stream.close();
        } catch (FileNotFoundException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("An error occured while saving.");
            alert.setContentText("Cannot write to this file. Is it already openFile by another software?");
            alert.showAndWait();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
