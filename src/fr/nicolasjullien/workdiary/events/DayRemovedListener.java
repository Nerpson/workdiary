package fr.nicolasjullien.workdiary.events;

import fr.nicolasjullien.workdiary.model.Day;

public interface DayRemovedListener {

    class DayRemovedEvent {
        public final Day day;

        public DayRemovedEvent(Day day) {
            this.day = day;
        }
    }

    void onDayRemoved(DayRemovedEvent event);
}
