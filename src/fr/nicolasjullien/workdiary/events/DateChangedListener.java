package fr.nicolasjullien.workdiary.events;

import java.time.LocalDate;

public interface DateChangedListener {

    class DateChangedEvent {
        public final LocalDate date;

        public DateChangedEvent(LocalDate date) {
            this.date = date;
        }
    }

    void onDateChanged(DateChangedEvent event);
}
