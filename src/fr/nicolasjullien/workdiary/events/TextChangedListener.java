package fr.nicolasjullien.workdiary.events;

public interface TextChangedListener {

    class TextChangedEvent {
        public final String text;

        public TextChangedEvent(String text) {
            this.text = text;
        }
    }

    void onTextChanged(TextChangedEvent event);
}
