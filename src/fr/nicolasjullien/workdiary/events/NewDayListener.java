package fr.nicolasjullien.workdiary.events;

import fr.nicolasjullien.workdiary.model.Day;

public interface NewDayListener {

    class NewDayEvent {
        public final Day day;

        public NewDayEvent(Day day) {
            this.day = day;
        }
    }

    void onNewDay(NewDayEvent event);

}
