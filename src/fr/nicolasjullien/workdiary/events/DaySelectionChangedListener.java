package fr.nicolasjullien.workdiary.events;

import fr.nicolasjullien.workdiary.model.Day;

public interface DaySelectionChangedListener {

    class DaySelectionChangedEvent {
        public final Day day;

        public DaySelectionChangedEvent(Day selectedDay) {
            this.day = selectedDay;
        }

        public DaySelectionChangedEvent() {
            this(null);
        }
    }

    void onDaySelectionChanged(DaySelectionChangedEvent event);
}
