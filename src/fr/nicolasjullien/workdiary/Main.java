package fr.nicolasjullien.workdiary;

import fr.nicolasjullien.workdiary.ui.DaysPane;
import fr.nicolasjullien.workdiary.ui.EditPane;
import fr.nicolasjullien.workdiary.ui.menu.MenuBar;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class Main extends Application {

    private MenuBar menuBar;
    private DaysPane daysPane;
    private EditPane editPane;

    @Override
    public void start(Stage primaryStage) throws Exception {
        Controller.getInstance().init(primaryStage);
        BorderPane root = new BorderPane();

        this.menuBar = new MenuBar();
        this.daysPane = new DaysPane();
        this.editPane = new EditPane();

        ScrollPane scrollPane = new ScrollPane(this.daysPane);
        scrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);
        scrollPane.setFitToWidth(true);
        scrollPane.setOnMouseClicked(e -> {
            if (!e.isConsumed()) Controller.getInstance().selectDay(null);
        });

        root.setTop(this.menuBar);
        root.setCenter(scrollPane);
        root.setBottom(this.editPane);

        primaryStage.setTitle("WorkDiary");
        primaryStage.setScene(new Scene(root, 600, 475));
        primaryStage.show();
    }




    public static void main(String[] args) {
        launch(args);
    }
}
