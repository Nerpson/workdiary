package fr.nicolasjullien.workdiary;

import fr.nicolasjullien.workdiary.model.Day;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class ReaderVersion1 {
    public List<Day> readRestOfStream(FileInputStream stream) throws IOException {

        ByteBuffer shortBuffer = ByteBuffer.allocate(Short.BYTES);

        byte[] bytesDaysCount = new byte[Short.BYTES];
        short daysCount;

        stream.read(bytesDaysCount);
        shortBuffer.put(bytesDaysCount);
        daysCount = shortBuffer.getShort(0);

        System.out.println("Il y a " +  daysCount + " jour(s) stockés dans ce fichier !");

        List<Day> days = new ArrayList<>();

        for (short i = 0; i < daysCount; ++i) {
            short year;
            byte[] bytesYear = new byte[Short.BYTES];
            byte[] byteMonth = new byte[1];
            byte[] byteDayOfMonth = new byte[1];

            LocalDate date;

            byte[] bytesDoneBytesCount = new byte[Short.BYTES];
            short doneBytesCount;
            byte[] bytesDone;
            String done;
            byte[] bytesTodoBytesCount = new byte[Short.BYTES];
            short todoBytesCount;
            byte[] bytesTodo;
            String todo;

            // Year
            stream.read(bytesYear);
            shortBuffer.clear();
            shortBuffer.put(bytesYear);
            year = shortBuffer.getShort(0);

            // Month
            stream.read(byteMonth);

            // Day
            stream.read(byteDayOfMonth);

            // Done
            stream.read(bytesDoneBytesCount);
            shortBuffer.clear();
            shortBuffer.put(bytesDoneBytesCount);
            doneBytesCount = shortBuffer.getShort(0);
            bytesDone = new byte[doneBytesCount];
            stream.read(bytesDone);
            done = new String(bytesDone, Charset.forName("UTF-16"));

            // Todo
            stream.read(bytesTodoBytesCount);
            shortBuffer.clear();
            shortBuffer.put(bytesTodoBytesCount);
            todoBytesCount = shortBuffer.getShort(0);
            bytesTodo = new byte[todoBytesCount];
            stream.read(bytesTodo);
            todo = new String(bytesTodo, Charset.forName("UTF-16"));

            date = LocalDate.of(year, byteMonth[0], byteDayOfMonth[0]);
            days.add(new Day(date, done, todo));
        }

        return days;
    }
}
