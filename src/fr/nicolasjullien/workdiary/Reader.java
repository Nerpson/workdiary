package fr.nicolasjullien.workdiary;

import fr.nicolasjullien.workdiary.exceptions.UnknownFileVersionException;
import fr.nicolasjullien.workdiary.model.Day;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

public class Reader {

    private final File file;

    public Reader(File file) {
        this.file = file;
    }

    public List<Day> read() {
        List<Day> days = null;
        try (FileInputStream stream = new FileInputStream(this.file)) {
            byte[] byteVersion = new byte[1];
            stream.read(byteVersion);
            switch (byteVersion[0]) {
                case 1:
                    ReaderVersion1 readerVersion1 = new ReaderVersion1();
                    days = readerVersion1.readRestOfStream(stream);
                    break;
                default:
                    throw new UnknownFileVersionException();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.err.println("File not found!");
        } catch (IOException e) {
            e.printStackTrace();
            System.err.println("An unknown error occurred!");
        }

        return days;
    }
}
