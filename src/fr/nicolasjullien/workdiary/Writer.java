package fr.nicolasjullien.workdiary;

import java.io.File;

public abstract class Writer {

    protected final File file;

    public Writer(File file) {
        this.file = file;
    }

    public abstract void write();

}
