package fr.nicolasjullien.workdiary;

import fr.nicolasjullien.workdiary.events.DayRemovedListener;
import fr.nicolasjullien.workdiary.events.DaySelectionChangedListener;
import fr.nicolasjullien.workdiary.events.NewDayListener;
import fr.nicolasjullien.workdiary.model.Day;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.util.*;

public class Controller {

    private static Controller instance;

    public static Controller getInstance() {
        if (instance == null) instance = new Controller();
        return instance;
    }

    private Stage primaryStage;
    private File file;
    private List<Day> days;
    private Day selectedDay;

    private Queue<NewDayListener> newDayListeners;
    private Queue<DayRemovedListener> dayRemovedListeners;
    private Queue<DaySelectionChangedListener> daySelectionChangedListener;

    public void init(Stage primaryStage) {
        this.primaryStage = primaryStage;
    }

    private Controller() {
        this.days = new ArrayList<>();
        this.newDayListeners = new ArrayDeque<>();
        this.dayRemovedListeners = new ArrayDeque<>();
        this.daySelectionChangedListener = new ArrayDeque<>();
    }

    public void addDay() {
        Day day = new Day();
        if (this.days.add(day)) {
            NewDayListener.NewDayEvent event = new NewDayListener.NewDayEvent(day);
            this.newDayListeners.forEach(l -> l.onNewDay(event));
        }
    }

    public void removeDay(Day day) {
        if (this.days.remove(day)) {
            if (this.getSelectedDay() == day) this.selectDay(null);
            DayRemovedListener.DayRemovedEvent event = new DayRemovedListener.DayRemovedEvent(day);
            this.dayRemovedListeners.forEach(l -> l.onDayRemoved(event));
        }
    }

    public void removeAllDays() {
        if (this.days.size() > 0) {
            this.days.clear();
            DayRemovedListener.DayRemovedEvent event = new DayRemovedListener.DayRemovedEvent(null);
            this.dayRemovedListeners.forEach(l -> l.onDayRemoved(event));
        }
    }

    public void selectDay(Day day) {
        if (this.selectedDay != day && (day == null || this.days.contains(day))) {
            this.selectedDay = day;
            DaySelectionChangedListener.DaySelectionChangedEvent event = new DaySelectionChangedListener.DaySelectionChangedEvent(day);
            this.daySelectionChangedListener.forEach(l -> l.onDaySelectionChanged(event));
        }
    }

    public boolean isDaySelected() {
        return this.selectedDay != null;
    }

    public List<Day> getDays() {
        return Collections.unmodifiableList(this.days);
    }

    public void addNewDayListener(NewDayListener listener) {
        this.newDayListeners.add(listener);
    }

    public void addDayRemovedListener(DayRemovedListener listener) {
        this.dayRemovedListeners.add(listener);
    }

    public void addDaySelectionChangedListener(DaySelectionChangedListener listener) {
        this.daySelectionChangedListener.add(listener);
    }

    public Day getSelectedDay() {
        return this.selectedDay;
    }

    private void clear() {
        System.out.println("Clearing");
        this.selectDay(null);
        this.removeAllDays();
        this.file = null;
    }

    public void newFile() {
        if (this.file != null) {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Question");
            alert.setHeaderText("Some changes were not saved yet.");
            alert.setContentText("Create new file anyway?");
            alert.showAndWait();

            if (!ButtonType.OK.equals(alert.getResult())) {
                return;
            }
        }
        this.clear();
    }

    public void openFile() {
        if (this.file != null) {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Question");
            alert.setHeaderText("Some changes were not saved yet.");
            alert.setContentText("Open another file anyway?");
            alert.showAndWait();

            if (!ButtonType.OK.equals(alert.getResult())) {
                return;
            }
        }

        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("WorkDiary files", "*.wkd")
        );
        File selectedFile = fileChooser.showOpenDialog(this.primaryStage);
        if (selectedFile == null) return;
        this.clear();
        this.file = selectedFile;

        Reader reader = new Reader(this.file);
        this.days = reader.read();
        NewDayListener.NewDayEvent event = new NewDayListener.NewDayEvent(null);
        this.newDayListeners.forEach(l -> l.onNewDay(event));
    }

    public void saveFile(boolean forceChoseFile) {
        if (forceChoseFile || this.file == null) {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Save");
            fileChooser.getExtensionFilters().addAll(
                    new FileChooser.ExtensionFilter("WorkDiary files", "*.wkd")
            );
            File selectedFile = fileChooser.showSaveDialog(this.primaryStage);
            if (selectedFile == null) return;
            this.file = selectedFile;
        }

        System.out.println("Saving to " + this.file);
        Writer writer = new WriterVersion1(this.file);
        writer.write();
    }

    public void saveFile() {
        this.saveFile(false);
    }

    public void saveFileAs() {
        this.saveFile(true);
    }
}
