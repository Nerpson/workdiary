package fr.nicolasjullien.workdiary.model;

import fr.nicolasjullien.workdiary.events.TextChangedListener;

import java.util.ArrayDeque;
import java.util.Queue;

public class Done {
    private String text;
    private Queue<TextChangedListener> textChangedListeners;

    public Done(String doneText) {
        this.text = doneText;
        this.textChangedListeners = new ArrayDeque<>();
    }

    public Done() {
        this("");
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        if (text != null && !text.equals(this.text)) {
            this.text = text;
            TextChangedListener.TextChangedEvent event = new TextChangedListener.TextChangedEvent(text);
            this.textChangedListeners.forEach(l -> l.onTextChanged(event));
        }
    }

    public void addTextChangedListener(TextChangedListener listener) {
        this.textChangedListeners.add(listener);
    }
}
