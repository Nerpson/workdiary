package fr.nicolasjullien.workdiary.model;

import com.sun.istack.internal.NotNull;
import fr.nicolasjullien.workdiary.events.DateChangedListener;

import java.time.LocalDate;
import java.util.ArrayDeque;
import java.util.Queue;

public class Day {
    private LocalDate date;
    private Done done;
    private Todo todo;

    private Queue<DateChangedListener> dateChangedListeners;

    public Day(LocalDate date, String done, String todo) {
        this.date = date;
        this.done = new Done(done);
        this.todo = new Todo(todo);
        this.dateChangedListeners = new ArrayDeque<>();
    }

    public Day(LocalDate date) {
        this(date, "", "");
    }

    public Day() {
        this(LocalDate.now());
    }


    public LocalDate getDate() {
        return this.date;
    }

    public void setDate(@NotNull LocalDate date) {
        if (!this.date.equals(date)) {
            this.date = date;
            DateChangedListener.DateChangedEvent event = new DateChangedListener.DateChangedEvent(this.date);
            this.dateChangedListeners.forEach(l -> l.onDateChanged(event));
        }
    }

    public Done getDone() {
        return done;
    }

    public void setDone(Done done) {
        this.done = done;
    }

    public Todo getTodo() {
        return this.todo;
    }

    public void setTodo(Todo todo) {
        this.todo = todo;
    }

    public void addOnDateChangedListener(DateChangedListener listener) {
        this.dateChangedListeners.add(listener);
    }
}
