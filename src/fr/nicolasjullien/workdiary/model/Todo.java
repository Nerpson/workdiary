package fr.nicolasjullien.workdiary.model;

import fr.nicolasjullien.workdiary.events.TextChangedListener;

import java.util.ArrayDeque;
import java.util.Queue;

public class Todo {
    private String text;
    private Queue<TextChangedListener> textChangedListeners;

    public Todo(String todoText) {
        this.text = todoText;
        this.textChangedListeners = new ArrayDeque<>();
    }

    public Todo() {
        this("");
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        if (text != null && !text.equals(this.text)) {
            this.text = text;
            TextChangedListener.TextChangedEvent event = new TextChangedListener.TextChangedEvent(text);
            this.textChangedListeners.forEach(l -> l.onTextChanged(event));
        }
    }

    public void addTextChangedListener(TextChangedListener listener) {
        this.textChangedListeners.add(listener);
    }
}
